import {defineStore} from "pinia";

import data from '../assets/data.csv'
import {RealEstate} from "../domain/RealEstate";
import {computed, ref} from "vue";

function randomImage () {
  return `image${Math.round(Math.random() * 20)}.jpeg`
}

export const useRealEstateStore = defineStore('realEstate', () => {
  const _entries = ref<RealEstate[]>(data.map((item: RealEstate) => {
    item.images = [
      randomImage(),
      randomImage(),
      randomImage()
    ]
    return item
  }))
  const entries = computed(() => _entries.value)

  function getById(id: string) {
    const entry = entries.value.find((item) => item.uid === id)

    return entry ?? null
  }

  return { entries, getById }
})
