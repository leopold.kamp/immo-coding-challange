export interface RealEstate {
  uid: string
  address: string
  price: number
  sqm: number
  bathrooms: number
  rooms: number
  energyEfficiencyClass: 'A+' | 'A' | 'B' | 'C' | 'D' | 'E' | 'F'
  description: string
  locationDescription: string,
  images: string[]
}
