
export interface RealEstateFilters {
  priceFrom?: number
  priceTo?: number
  roomsFrom?: number
  roomsTo?: number
  bathroomsFrom?: number
  bathroomsTo?: number
  sqmFrom?: number
  sqmTo?: number
}
