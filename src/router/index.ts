/**
 * router/index.ts
 *
 * Automatic routes for `./src/pages/*.vue`
 */

// Composables
import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'Home',
      component: () => import('@/pages/index.vue')
    },
    {
      path: '/real-estate/:id',
      name: 'RealEstateDetails',
      component: () => import('@/pages/RealEstateDetails.vue'),
      props: (route) => ({id: route.params.id})
    }
  ]
})

export default router
