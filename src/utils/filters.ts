export const filterGreaterEqThan = (value: number, against?: number) => {
  return (!against && against !== 0) || value >= against
}
export const filterSmallerEqThan = (value: number, against?: number) => {
  return (!against && against !== 0) || value <= against
}
