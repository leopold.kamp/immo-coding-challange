const formatter = new Intl.NumberFormat('de-DE', {
  style: 'currency',
  currency: 'EUR',
  maximumFractionDigits: 0
});
export const price = (price: number) => {
  return formatter.format(price)
}
